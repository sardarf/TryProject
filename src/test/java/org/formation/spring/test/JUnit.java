package org.formation.spring.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.formation.spring.config.ApplicationConfig;
import org.formation.spring.dao.CrudClientDAO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;




@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ApplicationConfig.class)
public class JUnit {

	@Autowired
	private CrudClientDAO crudClientDAO;
	
	
	@Test
	public void test() {
		assertNotNull(crudClientDAO);
		
		assertEquals(crudClientDAO.findOne(5).getId(), 5);
		
		assertEquals(crudClientDAO.findAll().size(), 6);
		
		assertEquals(crudClientDAO.findClientByNom("Lancaster").size(), 1);
		
		assertEquals(crudClientDAO.findAllClientByLogin("five").size(), 1);
	
		
		
	}
	
//	@Test
//	public void test() {
//		assertNotNull(serv.listClients());
//	}

}
